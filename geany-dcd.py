import geany
import gtk
import subprocess
import os

class DLangCompletionEngine:
    def __init__(self):
        pass

    def complete(self, path, text, position, file_name):
        dcd_client = subprocess.Popen(["/home/jbl/.local/bin/dcd-client",
                                       "-c", str(position)],
                                       stdin=subprocess.PIPE,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
        stdout, stderr = dcd_client.communicate(text)
        return stdout


class DLangCompletionPlugin(geany.Plugin):

    __plugin_name__ = "D Language completion"
    __plugin_version__ = "1.0"
    __plugin_description__ = "D Language completion plugin using the D Completion Daemon (DCD)"
    __plugin_author__ = "Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>"

    def __init__(self):
        geany.Plugin.__init__(self)
        self.menu_item = gtk.MenuItem("D completion using DCD")
        agr = gtk.AccelGroup()
        geany.main_widgets.window.add_accel_group(agr)
        key, mod = gtk.accelerator_parse("<Control><Alt>d")
        self.menu_item.add_accelerator("activate", agr, key, mod, gtk.ACCEL_VISIBLE)
        geany.main_widgets.tools_menu.append(self.menu_item)
        self.menu_item.show()
        self.menu_item.connect("activate", self.on_complete_clicked)
        self.engine = DLangCompletionEngine()

    def cleanup(self):
        self.menu_item.destroy()

    def parse_completions(self, completions):
        res = completions.split('\n')
        if len(res) > 0:
            res = res[1:]
            res = '\n'.join([x.split('\t')[0] for x in res[0:-1]]) + res[-1]
        else:
            res = "bla\nblabla"
        return res

    def on_complete_clicked(self, widget, data=None):
        current_doc = geany.document.get_current()
        cursor_position = current_doc.editor.scintilla.get_current_position()
        contents = current_doc.editor.scintilla.get_contents()
        raw_completions = self.engine.complete(".", contents, cursor_position, current_doc.file_name)
        completions = self.parse_completions(raw_completions)
        current_doc.editor.scintilla.send_text_message(2100, 0, completions)
