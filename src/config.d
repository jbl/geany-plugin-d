// The MIT License (MIT)
//
// Copyright (c) 2015 Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

module utils.config;

import std.array;
import std.conv;
import std.string;
import std.json;
import std.file;
import std.path;

import gtk.Entry;
import gtk.Label;
import gtk.CheckButton;
import gtk.Widget;
import gtk.HBox;
import gtk.VBox;
import gtk.ToggleButton;
import gtk.Dialog;
import gtk.ComboBox;
import glib.Internationalization;

import ilog;

class Config {
    private {
        Entry beautifier_path_entry;
        CheckButton verbose_cb;
        CheckButton beautify_code_cb;
        version (Posix) {
            CheckButton shebang_exec_cb;
        }
        Entry beautifier_config_entry;
        VBox vbox;
        Dialog dialog;
        ComboBox beautifier_combo;
        string _code_beautifier;
        ILog logger;
        immutable(string) default_config = import("geany-plugin-d.json");
        JSONValue config_data;
        string config_dir;

        void read() {
            // Default config
            config_data = parseJSON(default_config);
            JSONValue user_config;
            try {
                // User config
                user_config = parseJSON(readText(buildPath(config_dir, "geany-plugin-d.json")));
                foreach(string k, v; user_config) {
                    config_data[k] = v;
                }
            } catch (FileException e) {
            }
        }

        void save() {
            try {
                write(buildPath(config_dir, "geany-plugin-d.json"), config_data.toPrettyString());
                logger.log("Wrote geany-plugin-d plugin config to: " ~buildPath(config_dir, "geany-plugin-d.json"));
            } catch (FileException e) {
            }
        }

        void construct() {
            vbox = new VBox(false, 6);

            verbose_cb = new CheckButton(Internationalization.dgettext("geany-plugin-d", "Verbose"));
            beautify_code_cb = new CheckButton(Internationalization.dgettext("geany-plugin-d", "Beautify code on save"));
            beautifier_combo = new ComboBox(true);
            version (Posix) {
                shebang_exec_cb = new CheckButton(Internationalization.dgettext("geany-plugin-d", "Ensure #! scripts are executable"));
            }
            foreach(string key, value; config_data["beautifiers"]) {
                beautifier_combo.appendText(key);
            }
            beautifier_combo.setActiveText(config_data["beautifier"].str);
            beautifier_combo.addOnChanged( &on_beautifier_combo_changed );
            Label beautifer_label = new Label(Internationalization.dgettext("geany-plugin-d", "Beautifier to use: "));
            beautifier_config_entry = new Entry();
            beautifier_config_entry.setText(config_data["beautifiers"][config_data["beautifier"].str]["config"].str);
            beautifier_path_entry = new Entry();
            beautifier_path_entry.setText(config_data["beautifiers"][config_data["beautifier"].str]["path"].str);
            HBox hbox = new HBox(false, 6);
            hbox.packStart(verbose_cb, false, true, 0);
            version (Posix) {
                hbox.packStart(shebang_exec_cb, false, true, 0);
            }
            vbox.packStart(hbox, false, false, 0);

            hbox = new HBox(false, 6);
            hbox.packStart(beautify_code_cb, true, true, 0);
            vbox.packStart(hbox, false, false, 0);

            hbox = new HBox(false, 6);
            hbox.packStart(beautifer_label, false, false, 0);
            hbox.packStart(beautifier_combo, false, false, 0);
            vbox.packStart(hbox, false, false, 0);

            hbox = new HBox(false, 6);
            Label beautifer_path_label = new Label(Internationalization.dgettext("geany-plugin-d", "Path: "));
            hbox.packStart(beautifer_path_label, false, false, 0);
            hbox.packStart(beautifier_path_entry, true, true, 0);
            vbox.packStart(hbox, false, false, 0);
            hbox = new HBox(false, 6);
            Label beautifer_args_label = new Label(Internationalization.dgettext("geany-plugin-d", "Arguments: "));
            hbox.packStart(beautifer_args_label, false, false, 0);
            hbox.packStart(beautifier_config_entry, true, true, 0);
            vbox.packStart(hbox, false, false, 0);

            beautifier_combo.setSensitive(beautify_code);
            beautifier_config_entry.setSensitive(beautify_code);
            beautifier_path_entry.setSensitive(beautify_code);
            beautify_code_cb.setActive(config_data["beautify_code_on_save"].type == JSON_TYPE.TRUE);
            beautify_code_cb.addOnToggled(&on_beautify_code_changed);

            verbose_cb.setActive(config_data["verbose"].type == JSON_TYPE.TRUE);
            verbose_cb.addOnToggled(&on_verbose_changed);

            version (Posix) {
                shebang_exec_cb.setActive(config_data["ensure_shebang_exec"].type == JSON_TYPE.TRUE);
                shebang_exec_cb.addOnToggled(&on_shebang_exec_changed);
            }
            vbox.showAll();
        }

        void on_verbose_changed(ToggleButton b) {
            config_data["verbose"] = to !bool(b.getActive());
        }

        void on_beautify_code_changed(ToggleButton b) {
            bool isActive = to !bool(b.getActive());
            config_data["beautify_code_on_save"] = isActive;
            beautifier_combo.setSensitive(isActive);
            beautifier_config_entry.setSensitive(isActive);
            beautifier_path_entry.setSensitive(isActive);
        }

        void on_shebang_exec_changed(ToggleButton b) {
            bool isActive = to !bool(b.getActive());
            config_data["ensure_shebang_exec"] = isActive;
        }

        void on_beautifier_combo_changed(ComboBox cb) {
            config_data["beautifier"] = cb.getActiveText();
            beautifier_config_entry.setText(config_data["beautifiers"][config_data["beautifier"].str]["config"].str);
            beautifier_path_entry.setText(config_data["beautifiers"][config_data["beautifier"].str]["path"].str);
        }

        void on_dialog_response(int response, Dialog dlg) {
            switch(response) {
                case ResponseType.APPLY:
                    config_data["beautifiers"][config_data["beautifier"].str]["config"] = beautifier_config_entry.getText();
                    config_data["beautifiers"][config_data["beautifier"].str]["path"] = beautifier_path_entry.getText();
                    break;
                case ResponseType.OK:
                    config_data["beautifiers"][config_data["beautifier"].str]["config"] = beautifier_config_entry.getText();
                    config_data["beautifiers"][config_data["beautifier"].str]["path"] = beautifier_path_entry.getText();
                    save();
                    break;
                case ResponseType.CANCEL:
                    break;
                default:
                    break;
            }
        }

    }

    public {

        this(ILog logger, string config_dir) {
            this.logger = logger;
            this.config_dir = config_dir;
            read();
        }

        GtkWidget *getUi(GtkDialog *dialog) {
            this.dialog = new Dialog(dialog);
            construct();
            this.dialog.addOnResponse(&on_dialog_response);
            return cast(GtkWidget*) vbox.getVBoxStruct();
        }

        void verbose(bool newVerbose) @property {
            config_data["verbose"] = newVerbose;
        }

        bool verbose() @property {
            return config_data["verbose"].type == JSON_TYPE.TRUE;
        }

        void beautify_code(bool new_beautify_code) @property {
            config_data["beautify_code_on_save"] = new_beautify_code;
        }

        bool beautify_code() @property {
            return config_data["beautify_code_on_save"].type == JSON_TYPE.TRUE;
        }

        string beautifier() @property {
            return config_data["beautifier"].str;
        }

        string beautifier_path() @property {
            return config_data["beautifiers"][beautifier]["path"].str;
        }

        string[] beautifier_config(string beautifier) @property {
            string b_config = config_data["beautifiers"][beautifier]["config"].str;
            return split(b_config, ' ');
        }

        bool ensure_shebang_exec() @property {
            return config_data["ensure_shebang_exec"].type == JSON_TYPE.TRUE;
        }
    }

}
