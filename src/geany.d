// The MIT License (MIT)
//
// Copyright (c) 2015 Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/**
 * This module is a translation of the Geany C plugin interface.
 *
 * By importing Gtk (from the $(LINK2 http://gtkd.org/, GtkD project)), the conversion
 * of the plugindata.h header is fairly straightforward as types are already aliased.
 */
module geany;

import gtk.Widget;

/**
 * Interface to be implemented by D plugins
 *
 * This is basically the Geany C plugin interface wrapped in a thin D interface.
 *
 * You do not need to implement all methods, see the documentation for the ones that must be.
 *
 * See_Also:
 *  SamplePlugin
 */
interface GeanyDPlugin {

/**
 * Geany's plugin framework will call this method to let you know you can begin doing your thing.
 *
 * Params:
 *  data = GeanyData object populated by Geany
 *
 * See_Also:
 *  GeanyData
 */
    void initialize(GeanyData *data);
/**
 * Geany's plugin framework will call this method to let you know it's time to stop doing your thing.
 * Most likely, the user ticked off your plugin from the Plugin Manager :(
 */
    void cleanup();

/**
 * Geany's plugin framework will call this method to obtain information about your plugin.
 *
 * Params:
 *  info = a preallocated structure that you need to fill in
 */
    void set_info(PluginInfo *info);
/**
 * Geany's plugin framework will call this method to give you a chance to bail out if your plugin does not support the passed
 * ABI version.
 *
 * Params:
 *  abi_ver = Geany ABI version
 */
    int version_check(int abi_ver);
/**
 * Geany's plugin framework will call this method to allow your plugin to show its configuration panel.
 *
 * The dialog parameter is the C opaque pointer to the underlying Gtk dialog widget. If you want to use this dialog as a GtkD object,
 * you need to contruct one first using the appropriate GtkD Dialog contructor.
 *
 * Examples:
 * -------------------
 * GtkWidget *configure(GtkDialog *dialog) {
 *     this.dialog = new Dialog(dialog);
 *     ...
 * }
 * -------------------
 *
 * Params:
 *  dialog = the parent GtkDialog that will embed your plugin's configuration panel
 */
    GtkWidget *configure(GtkDialog *dialog);

/**
 * Geany's plugin framework will call this method whenever the user clicks on the Help button from Geany's Plugin Manager.
 */
    void help();
}

extern (C) GeanyDPlugin get_d_plugin();

extern (C)
{
    /**
     * This is the type passed by Geany to the plugin in the plugin_init() function.
     *
     * See_Also:
     *  GeanyDPlugin.set_info
     */
    struct PluginInfo {
        /** The name of the plugin. */
        gchar *name;
        /** The description of the plugin. */
        gchar *description;
        /** The version of the plugin. 
         * Note: version is a reserved keyword in D, so map it to "_version"
         */
        gchar *_version;
        /** The author of the plugin. */
        gchar *author;
    }

    /**
     * Info about our plugin.
     *
     * See_also:
     */
    struct GeanyPlugin
    {
        PluginInfo    *info;    /** Fields set in plugin_set_info(). */ /**<  */
    }

    /**
     * @brief
     *
     *
     */
    struct PluginCallback
    {
        /** The name of signal, must be an existing signal. For a list of available signals,
         *  please see the @link pluginsignals.c Signal documentation @endlink. */
        const gchar* signal_name;
        /** A callback function which is called when the signal is emitted. */
        GCallback callback;
        /** Set to TRUE to connect your handler with g_signal_connect_after(). */
        bool after;
        /** The user data passed to the signal handler. */
        void* user_data;
    }

    struct ScintillaObject;

    /**
     * @brief
     *
     *
     */
    struct GeanyEditor {
        GeanyDocument    *document;        /**< The document associated with the editor. */ /**<  */
        ScintillaObject    *sci; /**<  */
    }

    /**
     * @brief
     *
     *
     */
    struct GeanyDocument
    {
        /** General flag to represent this document is active and all properties are set correctly. */
        bool is_valid; /**<  */
        int index;                    /**< Index in the documents array. */ /**<  */
        /** Whether this document supports source code symbols(tags) to show in the sidebar. */
        bool has_tags; /**<  */
        /** The UTF-8 encoded file name.
         * Be careful; glibc and GLib file functions expect the locale representation of the
         * file name which can be different from this.
         * For conversion into locale encoding, you can use @ref utils_get_locale_from_utf8().
         * @see real_path. */
        gchar             *file_name; /**<  */
        /** The encoding of the document, must be a valid string representation of an encoding, can
         *  be retrieved with @ref encodings_get_charset_from_index. */
        gchar             *encoding; /**<  */
        /** Internally used flag to indicate whether the file of this document has a byte-order-mark. */
        gboolean has_bom; /**<  */

        GeanyEditor *editor;    /**< The editor associated with the document. */ /**<  */
    }
    /**
     * @brief
     *
     *
     */
    struct GeanyMainWidgets
    {
        GtkWindow   *window;            /**< Main window. */ /**<  */
        GtkWidget   *toolbar;           /**< Main toolbar. */ /**<  */
        GtkWidget   *sidebar_notebook;  /**< Sidebar notebook. */ /**<  */
        GtkWidget   *notebook;          /**< Document notebook. */ /**<  */
        GtkWidget   *editor_menu;       /**< Popup editor menu. */ /**<  */
        GtkWidget   *tools_menu;        /**< Most plugins add menu items to the Tools menu. */ /**<  */
        /** Progress bar widget in the status bar to show progress of various actions.
         * See ui_progress_bar_start() for details. */
        GtkWidget   *progressbar; /**<  */
        GtkWidget   *message_window_notebook; /**< Message Window notebook. */ /**<  */
        /** Plugins modifying the project can add their items to the Project menu. */
        GtkWidget   *project_menu; /**<  */
    }
    /**
     * @brief
     *
     *
     */
    struct GeanyData {
        GeanyApp *app; /**<  */
        GeanyMainWidgets *widgets; /**<  */
    }
    //~ struct TMWorkspace;
    //~ struct GeanyProject;
    /**
     * @brief
     *
     *
     */
    struct GeanyApp
    {
        gboolean debug_mode; /**<  */
        gchar               *configdir; /**<  */
        gchar               *datadir; /**<  */
        gchar               *docdir; /**<  */
        //~ TMWorkspace   *tm_workspace;
        //~ GeanyProject *project;
    }

    void msgwin_status_add (const gchar *format,...);
    long scintilla_send_message    (ScintillaObject *sci,uint iMessage, ulong wParam, long lParam);
    void main_locale_init(const gchar* locale_dir, const gchar *pkg);
    /**
     * @brief
     *
     *
     */
    extern(C) alias GeanyKeyGroupCallback = gboolean function(guint);
    /**
     * @brief
     *
     *
     */
    extern(C) alias GeanyKeyCallback = void function (guint key_id);
    /**
     * @brief
     *
     *
     */
    struct GeanyKeyGroupInfo
    {
        const gchar *name;        /**< Group name used in the configuration file, such as @c "html_chars" */ /**<  */
        gsize count;            /**< The number of keybindings the group will hold */ /**<  */
    }
    struct GeanyKeyGroup;
    /**
     * @brief
     *
     *
     */
    struct GeanyKeyBinding
    {
        guint key;                /**< Key value in lower-case, such as @c GDK_a or 0 */ /**<  */
        GdkModifierType mods;    /**< Modifier keys, such as @c GDK_CONTROL_MASK or 0 */ /**<  */
        gchar *name;            /**< Key name for the configuration file, such as @c "menu_new" */ /**<  */
        /** Label used in the preferences dialog keybindings tab.
         * May contain underscores - these won't be displayed. */
        gchar *label; /**<  */
        /** Function called when the key combination is pressed, or @c NULL to use the group callback
         * (preferred). @see plugin_set_key_group(). */
        GeanyKeyCallback callback; /**<  */
        GtkWidget *menu_item;    /**< Optional widget to set an accelerator for, or @c NULL */ /**<  */
        guint id; /**<  */
        guint default_key; /**<  */
        GdkModifierType default_mods; /**<  */
    }
    GeanyKeyGroup *plugin_set_key_group(GeanyPlugin *plugin,
                                        const gchar *section_name, gsize count, GeanyKeyGroupCallback callback);
    GeanyKeyBinding *keybindings_set_item(GeanyKeyGroup *group, gsize key_id,
                                          GeanyKeyCallback callback, guint key, GdkModifierType mod,
                                          const gchar *name, const gchar *label, GtkWidget *menu_item);

    GtkWidget *ui_lookup_widget(GtkWidget *widget, const gchar *widget_name);

    version(Geany_before_125) {
        struct GeanyFiletype;
        /**
         * @brief
         *
         *
         */
        struct DocumentFuncs
        {
            GeanyDocument*   function (const gchar *utf8_filename,  GeanyFiletype *ft, const gchar *text) document_new_file; /**<  */
            GeanyDocument*   function () document_get_current; /**<  */
        }
        /**
         * @brief
         *
         *
         */
        struct GeanyFunctions
        {
            DocumentFuncs        *doc_func;        /**< See document.h */ /**<  */
        }
        GeanyFunctions *geany_functions;
        /**
         *
         * Returns: pointer to a GeanyDocument structure representing the current document.
         *
         */
        GeanyDocument *document_get_current () {
            return geany_functions.doc_func.document_get_current();
        }
    } else {
        GeanyDocument *document_get_current();
    }

//**************************************************************************************************
// Proxy functions that call out to your actual GeanyDPlugin interface implementation
//**************************************************************************************************
    /**
     * Geany calls this function when populating the plugin list in the plugin manager (I think) to determine the
     * compatibility of your plugin with the Geany version.
     *
     * This implementation is simply a proxy to your own plugin through the GeanyDPlugin interface.
     *
     * Params:
     *    abi_ver = Geany's AbI version
     * Returns: the ABI this plugin is compatible with.
     *
     */
    int plugin_version_check(int abi_ver)
    {
        return get_d_plugin().version_check(abi_ver);
    }
    /**
     * Geany calls this function when the user clicks on the Help button for your plugin in the plugin manager.
     *
     * This implementation is simply a proxy to your own plugin through the GeanyDPlugin interface.
     * See_Also:
     *    GeanyDPlugin, get_d_plugin
     */
    void plugin_help() {
        get_d_plugin().help();
    }

    /**
     * Geany calls this function when the user clicks on the Preferences button for your plugin in the plugin manager.
     *
     * This implementation is simply a proxy to your own plugin through the GeanyDPlugin interface.
     *
     * Params:
     *      dialog = a pointer to the GtkDialog object that will contain your plugin's config UI
     * Returns:
     *      a pointer to a GtkWidget implementing your plugin's config UI
     * See_Also:
     *    GeanyDPlugin, get_d_plugin
     */
    GtkWidget *plugin_configure(GtkDialog *dialog) {
        return get_d_plugin().configure(dialog);
    }

    /**
     * Geany calls this function when the user disables your plugin through the plugin manager.
     *
     * This implementation is simply a proxy to your own plugin through the GeanyDPlugin interface.
     * See_Also:
     *    GeanyDPlugin, get_d_plugin
     */
    void plugin_cleanup()
    {
        get_d_plugin().cleanup();
    }

    /**
     * Geany calls this function when the user enables your plugin through the plugin manager.
     *
     * This implementation is simply a proxy to your own plugin through the GeanyDPlugin interface
     *
     * Params:
     *    data = pointer to a valid GeanyData structure
     *
     * See_Also:
     *    GeanyDPlugin, get_d_plugin, GeanyData
     *
     */
    void plugin_init(GeanyData *data)
    {
        get_d_plugin().initialize(data);
    }

    /**
     * Geany calls this function during its startup to obtain plugin information.
     * See_Also:
     *  GeanyDPlugin, PluginInfo
     */
    void plugin_set_info(PluginInfo *info)
    {
        get_d_plugin().set_info(info);
    }

    GeanyPlugin     *geany_plugin;

//**************************************************************************************************
// Some constants, mainly Scintilla message IDs
//
// Look at generating this at compile time at some point, probably using something like
// https://github.com/PhilippeSigaud/Pegged
//**************************************************************************************************
    enum SCI_GETCURRENTPOS = 2008U;
    enum SCI_GOTOPOS = 2025U;
    enum SCI_SETTEXT = 2181U;
    enum SCI_GETTEXT = 2182U;
    enum SCI_GETTEXTLENGTH = 2183U;
}
