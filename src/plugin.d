// The MIT License (MIT)
//
// Copyright (c) 2015 Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import std.stdio;
import std.array;
import std.file;
import std.path;
import std.process;
import std.string;
import std.conv;
import std.json;
import std.algorithm.searching;
import std.functional;
import core.runtime;


import gtkc.gtk;
import gtk.HBox;
import gtk.VBox;
import gtk.Window;
import gtk.CheckButton;
import gtk.ToggleButton;
import gtk.MenuItem;
import gtk.Menu;
import gdk.Keymap;
import gdk.Keysyms;
import glib.Internationalization;

import geany;
import config;
import ilog;

/**
 * This is an example of a Geany Plugin written using the D language.
 *
 * See_Also: geany.GeanyDPlugin
 */
class SamplePlugin: geany.GeanyDPlugin, ilog.ILog {
    private {
        static immutable(string) PLUGIN_NAME = "geany-plugin-d";
        static immutable(string) PLUGIN_VERSION = "0.1";
        static immutable(string) PLUGIN_AUTHOR = "Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>";
        static string plugin_path = "~/.config/geany/plugins";

        config.Config cfg = null;

        static char * _(string msgid) {
            return cast(char *) toStringz(Internationalization.dgettext(PLUGIN_NAME, msgid));
        }
        static char * gettext(string msgid) {
            return cast(char *) toStringz(Internationalization.dgettext(PLUGIN_NAME, msgid));
        }
        MenuItem format_code;
    }

    public {
        /// This is the variable holding the singleton instance of our plugin
        static SamplePlugin instance = null;

        this() {
            version(Posix) {
                version (linux) {
                    import core.sys.linux.dlfcn;
                } else {
                    import core.sys.posix.dlfcn;
                }
                Dl_info info;
                plugin_path = expandTilde(plugin_path);
                if (dladdr(&get_d_plugin, &info) != 0) {
                    plugin_path = dirName(fromStringz(info.dli_fname).idup);
                }
            }
        }

        void initialize(GeanyData *data) {
            if (cfg is null) {
                cfg = new config.Config(this, to !(string)(fromStringz(data.app.configdir)));
            }
            GeanyKeyGroup *key_group;
            key_group = plugin_set_key_group(geany_plugin, cast(const char*) PLUGIN_NAME, 1, null);
            keybindings_set_item(key_group, 0UL, &SamplePlugin.on_keybinding_trempoline, GdkKeysyms.GDK_d, ModifierType.SHIFT_MASK|ModifierType.SUPER_MASK, cast(const char*) PLUGIN_NAME, "Geany Plugin in D", null);
            format_code = new MenuItem(&on_beautify_code_menu, "Beautify code");
            format_code.show();
            gtk_container_add(cast(GtkContainer *) data.widgets.editor_menu, cast(GtkWidget *) format_code.getMenuItemStruct());
        }

        void cleanup() {
            format_code.destroy();
            format_code = null;
            Runtime.terminate();
            SamplePlugin.instance = null;
        }

        char *info_name;
        char *info_description;
        void set_info(PluginInfo *info) {
            main_locale_init(toStringz(to !(string)(buildPath(plugin_path, "locale"))), cast(const char*) PLUGIN_NAME);
            info_name = gettext("Plugin written in D!");
            info.name = info_name;
            info_description = gettext("This is an example of a Geany plugin written using the D language");
            info.description = info_description;
            info._version = cast(char*) PLUGIN_VERSION;
            info.author = cast(char*) PLUGIN_AUTHOR;
        }

        int version_check(int abi_ver) {
            // TODO: figure out a better way than this magic number...
            return 216;
        }

        GtkWidget *configure(GtkDialog *dialog) {
            return cfg.getUi(dialog);
        }

        void help() {
        }
    }
    protected {

        void log(string msg) {
            if(cfg.verbose) {
                msgwin_status_add(toStringz(msg));
            }
        }

        extern (C) static void on_keybinding_trempoline(guint key_id) {
            if (instance !is null) {
                instance.on_keybinding();
            }
        }

        void on_keybinding() {
            try {
                GeanyDocument *doc = document_get_current();
                auto file_str = cast(string) (fromStringz(doc.file_name));
                switch(extension(file_str)) {
                    case ".json":
                        to_pretty_json(doc, file_str);
                        break;
                    case ".d":
                        to_pretty_d(doc, file_str);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                log("plugin exception: " ~e.toString());
            }
        }

        void on_document_save(GObject *o, GeanyDocument *doc, void *data) {
            version(Posix) {
                import core.sys.posix.sys.stat;
                if (cfg.ensure_shebang_exec) {
                    auto file_str = cast(string) (fromStringz(doc.file_name));
                    auto first_line = to !string(read(file_str, 2));
                    if (startsWith(first_line, "#!")) {
                        auto perms = getAttributes(file_str);
                        if (!(perms & S_IXUSR)) {
                            log("File is not user executable, making it executable...");
                            setAttributes(file_str, perms|S_IXUSR);
                        } else {
                            log("File is already user executable");
                        }
                    }
                }
            }
        }

        void on_document_before_save(GObject *o, GeanyDocument *doc, void *data) {
            if (cfg.beautify_code) {
                try {
                    auto file_str = cast(string) (fromStringz(doc.file_name));
                    switch(extension(file_str)) {
                        case ".json":
                            to_pretty_json(doc, file_str);
                            break;
                        case ".d":
                            to_pretty_d(doc, file_str);
                            break;
                        default:
                            break;
                    }
                } catch (Exception exc) {
                    log("plugin exception: " ~exc.toString());
                }
            }
        }

        void to_pretty_json(GeanyDocument *doc, string file_str) {
            long len = scintilla_send_message(doc.editor.sci, SCI_GETTEXTLENGTH, 0UL, 0L);
            char* buffer = new char[cast(uint)(len+1)].ptr;
            scintilla_send_message(doc.editor.sci, SCI_GETTEXT, len+1, cast(long) buffer);
            try {
                auto json = parseJSON(fromStringz(buffer));
                log("Pretty printing JSON for file " ~file_str);
                immutable char *ptr = toStringz(json.toPrettyString());
                scintilla_send_message(doc.editor.sci, SCI_SETTEXT, 0UL, cast(long) ptr);
            } catch (JSONException e) {
                log("Error parsing JSON in " ~file_str);
            }
        }

        void to_pretty_d(GeanyDocument *doc, string file_str) {
            log("Pretty printing D for file " ~file_str);

            // Get current caret position to roughly restore it after formatting...
            long current_position = scintilla_send_message(doc.editor.sci, SCI_GETCURRENTPOS, 0UL, 0L);
            long len = scintilla_send_message(doc.editor.sci, SCI_GETTEXTLENGTH, 0UL, 0L);
            char* buffer = new char[cast(uint) len+1].ptr;
            scintilla_send_message(doc.editor.sci, SCI_GETTEXT, len+1, cast(long) buffer);
            string beautifier = cfg.beautifier();
            string[] config = cfg.beautifier_config(beautifier);
            string path = cfg.beautifier_path();
            if (path != "") {
                config.insertInPlace(0, path);
            } else {
                config.insertInPlace(0, beautifier);
            }
            log(join(config, ' '));
            ProcessPipes pipes;
            try {
                pipes = pipeProcess(config);
                pipes.stdin().write(fromStringz(buffer));
                pipes.stdin().flush();
                pipes.stdin().close();
                string formatted = "";
                foreach (line; pipes.stdout.byLine) {
                    formatted ~= line ~"\n";
                }
                string stderr = "";
                foreach (line; pipes.stderr.byLine) {
                    stderr ~= line ~"\n";
                }
                if (wait(pipes.pid) == 0) {
                    immutable char *ptr = toStringz(formatted);
                    scintilla_send_message(doc.editor.sci, SCI_SETTEXT, 0UL, cast(long) ptr);

                    // Restore the caret position after formatting...
                    scintilla_send_message(doc.editor.sci, SCI_GOTOPOS, cast(ulong) current_position, 0L);
                } else {
                    log(stderr);
                }
            } catch (ProcessException e) {
                log("Error pretty printing: " ~e.toString());
            } catch (StdioException e) {
                log("Error pretty printing: " ~e.toString());
            } finally {
                if (pipes.pid !is null) {
                    wait(pipes.pid);
                }
            }
        }

        void on_beautify_code_menu(MenuItem menu_item) {
            on_keybinding();
        }
    }
}

/*
 * Geany D Plugin framework bit
 */
extern (C) {
    GeanyDPlugin get_d_plugin() {
        if(SamplePlugin.instance is null) {
            Runtime.initialize();
            SamplePlugin.instance = new SamplePlugin();
        }
        return SamplePlugin.instance;
    }

    /// Geany signals that we care about
    PluginCallback[3] plugin_callbacks =
    [
        { cast(char*) "document-save", cast(GCallback)&on_document_save_trempoline, false, null },
        { cast(char*) "document-before-save", cast(GCallback)&on_document_before_save_trempoline, false, null },
        { null, null, false, null }
    ];

    void on_document_save_trempoline(GObject *o, GeanyDocument *doc, void *data) {
        SamplePlugin plugin = cast(SamplePlugin) get_d_plugin();
        plugin.on_document_save(o, doc, data);
    }
    void on_document_before_save_trempoline(GObject *o, GeanyDocument *doc, void *data) {
        SamplePlugin plugin = cast(SamplePlugin) get_d_plugin();
        plugin.on_document_before_save(o, doc, data);
    }
}

int main() {
    return 0;
}

unittest {
    PluginInfo *i = new PluginInfo;
    plugin_set_info(i);
    assert (fromStringz(i.name) == "Plugin written in D");
}
