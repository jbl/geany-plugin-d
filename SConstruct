# The MIT License (MIT)
# 
# Copyright (c) 2015 Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from os.path import expanduser, join
from sys import maxsize

PLUGIN_NAME = 'geany-plugin-d'

env = Environment(SHOBJSUFFIX='.o', tools = ['default', 'dhelpers', 'gettext', 'gnuauto'])

SCONS_VARS_FILE = ".scons.vars.conf"

class ArchMap(dict):
    def __init__(self, *args, **kwargs):
        self.update(*args, **kwargs)
    def __getitem__(self, key):
        for k in self.keys():
            if key.find(k) != -1:
                return dict.__getitem__(self, k)
    def __repr__(self):
        dictrepr = dict.__repr__(self)
        return '%s(%s)' % (type(self).__name__, dictrepr)
    def get(self, k, default):
        res = self.__getitem__(k)
        if res != None:
            return res
        return default

opts = Variables(SCONS_VARS_FILE)
DefaultEnvironment().GNUAutoInit(opts=opts, PACKAGE_NAME=PLUGIN_NAME)
opts.Add(EnumVariable('build_type', 'debug/release', 'release', allowed_values=('debug', 'release')))
opts.Add(EnumVariable('arch', 'Architecture to build for', 'x86_64' if maxsize > 2147483647 else 'i386', allowed_values=('x86_64', 'i386'), map=ArchMap({'x86_64': 'x86_64', 'i386': 'i386'})))
opts.Update(env)
opts.Save(SCONS_VARS_FILE, env)
Help(opts.GenerateHelpText(env))

def CheckPKGConfig(context, version):
    context.Message( 'Checking for pkg-config... ' )
    ret = context.TryAction('pkg-config --atleast-pkgconfig-version=%s' % version)[0]
    context.Result( ret )
    return ret

def CheckPKG(context, name):
    context.Message( 'Checking for %s... ' % name )
    ret = context.TryAction('pkg-config --exists \'%s\'' % name)[0]
    context.Result( ret )
    return ret

def CheckGeanyVersion(context, version, geany_env):
    context.Message( 'Checking for Geany ABI/API version %s... ' % version)
    context.env.MergeFlags(geany_env)
    ret = context.TryRun('''#include <geany/geanyplugin.h>
    int main(int argc, char*argv[]) {
       printf("GEANY_API_VERSION: %d\\n", GEANY_API_VERSION);
       printf("GEANY_ABI_VERSION: %d", GEANY_ABI_VERSION);
       return 0;
    }
    ''', '.c')
    res = list(ret)
    if ret[0] == 1:
        geany_versions = dict([x.split(':') for x in ret[1].split('\n')])
        try:
            version = int(version)
            comparator = lambda x, y: x == y
        except ValueError:
            comparator = lambda x, y: eval(str(x) + y)
        geany_env['GEANY_API_VERSION'] = int(geany_versions['GEANY_API_VERSION'])
        if not comparator(int(geany_versions['GEANY_API_VERSION']), version):
            res[0] = 0
            context.Log('Wanted GEANY_API_VERSION: %s, found: %s\n'%(version, geany_versions['GEANY_API_VERSION']))
    context.Result(res[0])
    return res[0]

if not env.GetOption('clean') and not env.GetOption('help'):
    conf_env = Environment(tools = ['default'])
    conf = Configure(conf_env, custom_tests = {'CheckPKGConfig' : CheckPKGConfig,
                                               'CheckPKG' : CheckPKG,
                                               'CheckGeanyVersion' : CheckGeanyVersion })
    if not conf.CheckPKGConfig('0.15.0'):
        print 'pkg-config >= 0.15.0 not found, check config.log for details'
        Exit(1)

    if not conf.CheckPKG('geany >= 1.20'):
        print('geany >= 1.20 not found, check config.log for details')
        Exit(1)

    if not conf.CheckPKG('gtkd1 >= 1.7.4'):
        print('gtkd1 >= 1.7.4 not found, check config.log for details')
        Exit(1)

    geany_params = env.ParseFlags('!pkg-config --cflags --libs geany')

    if not conf.CheckGeanyVersion('<= 225', geany_params):
        print('GEANY_API_VERSION <= 225 not found, check config.log for details')
        Exit(1)
    else:
        if geany_params['GEANY_API_VERSION'] < 224:
            env.Append(DVERSIONS=['Geany_before_125'])

    conf.Finish()

env['POAUTOINIT'] = 1
env['POTDOMAIN'] = PLUGIN_NAME
env['XGETTEXTFLAGS'] = [
    '--no-location',
    '--package-name=%s' % PLUGIN_NAME,
    '--package-version=%s' % '1.0',
    ]
SConscript('src/po/SConscript.i18n', exports=['env', 'PLUGIN_NAME'])

build_dir = env.subst('build/${arch}/${build_type}')

VariantDir(build_dir, 'src', duplicate = 0)
SConscript(join(build_dir, 'SConscript'), exports=['env', 'PLUGIN_NAME'])
SConscript(join(build_dir, 'po', 'SConscript'), exports=['env', 'PLUGIN_NAME'])
env.Default(build_dir)
env.Default(join(build_dir, 'po'))

if 'doc' in COMMAND_LINE_TARGETS or 'install' in COMMAND_LINE_TARGETS:
    VariantDir('build/html', 'src', duplicate = 0)
    SConscript(['build/html/SConscript.ddoc'], exports=['env', 'PLUGIN_NAME'])


env.DistClean(SCONS_VARS_FILE)
env.DistClean('build')
