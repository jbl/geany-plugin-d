# geany-plugin-d


## About

`geany-plugin-d` is a show-case of a [geany](http://geany.org/) plugin implemented in the [D language](http://dlang.org/).

Currently, a small subset of the [Geany Plugin API](http://www.geany.org/manual/reference/) is exposed to plugins written in D, 
with the ultimate goal being to have access to the full API.

## Installation

See the INSTALL file.

## Usage

After installing the demo plugin, open the Plugin Manager in Geany and enable it there.

## Issues

Please report any issues related to geany-plugin-d using the [Bitbucket Issue Tracker](https://bitbucket.org/jbl/geany-plugin-d/issues).

## Development

## License

geany-plugin-d is covered by the MIT license, see the COPYING file.
