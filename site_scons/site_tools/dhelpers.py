from SCons.Script import *

def DMDParseConfigCallBack(env, pkg_config):
    args = pkg_config.split()
    for arg in args:
        if arg.startswith('-L') or arg.startswith('-defaultlib'):
            env.Append(DLINKFLAGS=arg)
        else:
            env.Append(DFLAGS=arg)
            #if arg[2:].startswith('-l'):
                #env.Append(LIBS=[arg[4:]])
            #elif arg[2:].startswith('-L'):
                #env.Append(LIBPATH=[arg[4:]])
        #elif arg.startswith('-I'):
            #env.Append(DPATH=[arg[2:]])
def DPkgConfig(env, source):
    env.ParseConfig(source, DMDParseConfigCallBack)

def CheckPKGConfig(context, version):
     context.Message( 'Checking for pkg-config... ' )
     ret = context.TryAction('pkg-config --atleast-pkgconfig-version=%s' % version)[0]
     context.Result( ret )
     return ret

def CheckPKG(context, name):
     context.Message( 'Checking for %s... ' % name )
     ret = context.TryAction('pkg-config --exists \'%s\'' % name)[0]
     context.Result( ret )
     return ret

def generate(env):
    action = '$DC $DFLAGS $_DINCFLAGS $_DVERFLAGS $_DDEBUGFLAGS -D -Df$TARGET $SOURCE -o-'
    bld = Builder(action = action,
                  suffix = '.html',
                  src_suffix = '.d')
    env.Append(BUILDERS = {'DDoc' : bld})
    env.AddMethod(DPkgConfig, "DPkgConfig")

def exists(env):
    return True
