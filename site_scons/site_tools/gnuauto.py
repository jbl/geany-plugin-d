from SCons.Script import *
from os import unlink
from os.path import expanduser, join, abspath, isabs

def DistClean(env, targets=[]):
    DIST_CLEAN_TARGETS = [".sconf_temp", "config.log"]
    env.Clean("distclean", DIST_CLEAN_TARGETS)
    # Just accumulate the clean targets in the alias
    env.Clean("distclean", targets)
    env.Alias("distclean")

def clean_sconsign():    
    if "distclean" in COMMAND_LINE_TARGETS and GetOption('clean'):
        try:
            unlink('.sconsign.dblite')
            print "Removed .sconsign.dblite"
        except Exception, e:
            pass

def GNUAutoInit(env, *args, **kwargs):
    gnu_dir_vars = ['prefix', 'exec_prefix', 'bindir', 'sbindir', 'libexecdir', 'libdir', 'datarootdir', 'datadir', 'sysconfdir', 'localedir', 'docdir']
    env.DistClean()
    if kwargs.has_key('opts'):
        opts = kwargs.pop('opts')
    else:
        opts = Variables()

    if kwargs.has_key('PACKAGE_NAME'):
        package_name = kwargs.pop('PACKAGE_NAME')
        env['PACKAGE_NAME'] = package_name
    else:
        if env.has_key('PACKAGE_NAME'):
            package_name = env['PACKAGE_NAME']
        else:
            package_name = 'DEFAULT_PACKAGE_NAME'
    
    opts.Add(PathVariable('DESTDIR', 'Installation root', '', PathVariable.PathAccept))
    opts.Add(PathVariable('prefix', 'A prefix used in constructing the default values of the variables listed below', '/usr/local', PathVariable.PathAccept))
    opts.Add(PathVariable('exec_prefix', 'A prefix used in constructing the default values of some of the variables listed below', '${prefix}', PathVariable.PathAccept))    
    opts.Add(PathVariable('bindir', 'The directory for installing executable programs that users can run', '${exec_prefix}/bin', PathVariable.PathAccept))
    opts.Add(PathVariable('sbindir', 'The directory for installing executable programs that can be run from the shell, but are only generally useful to system administrators', '${exec_prefix}/sbin', PathVariable.PathAccept))
    opts.Add(PathVariable('libexecdir', 'The directory for installing executable programs to be run by other programs rather than by users', '${exec_prefix}/libexec', PathVariable.PathAccept))
    opts.Add(PathVariable('libdir', 'A prefix used in constructing the default values of the variables listed below', '${exec_prefix}/lib', PathVariable.PathAccept))
    opts.Add(PathVariable('datarootdir', 'A prefix used in constructing the default values of the variables listed below', '${prefix}/share', PathVariable.PathAccept))
    opts.Add(PathVariable('datadir', 'A prefix used in constructing the default values of the variables listed below', '${datarootdir}', PathVariable.PathAccept))
    opts.Add(PathVariable('sysconfdir', 'A prefix used in constructing the default values of the variables listed below', '${prefix}/etc', PathVariable.PathAccept))
    opts.Add(PathVariable('localedir', 'A prefix used in constructing the default values of the variables listed below', '${datarootdir}/locale', PathVariable.PathAccept))
    opts.Add(PathVariable('docdir', 'The directory for installing documentation files (other than Info) for this package', '${datarootdir}/doc/${PACKAGE_NAME}', PathVariable.PathAccept))
    opts.Update(env)

def GNUInstall(env, target, source):
    if env['DESTDIR'] is not '':
        target = join(env['DESTDIR'], target)
    return env.Install(target, source)

def GNUInstallAs(env, target, source):
    if env['DESTDIR'] is not '':
        target = join(env['DESTDIR'], target[1:] if isabs(target) else target)
    return env.InstallAs(target, source)

def generate(env):
    import atexit
    AddMethod(DefaultEnvironment, GNUAutoInit)
    AddMethod(Environment, GNUAutoInit)
    AddMethod(DefaultEnvironment, DistClean)
    AddMethod(Environment, DistClean)
    AddMethod(Environment, GNUInstall)
    AddMethod(Environment, GNUInstallAs)
    atexit.register(clean_sconsign)
    env.DistClean()
    
def exists(env):
    return True
