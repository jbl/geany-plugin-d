# Requirements

- the [DMD D compiler](http://dlang.org/download.html)
- [Gtk D bindings](https://github.com/gtkd-developers/GtkD)
  - currently, only geany the GtkD for gtk2 bindings are supported.
    Furthermore, GtkD requires patching to allow shared libraries (such as a geany plugin) to be linked against it.
- the [SCons](http://www.scons.org/) build tool, tested with version 2.3.6
- [Geany](http://geany.org/) (duh!), tested with versions 1.23, 1.24 & 1.25

You may choose to add the [D-Apt](http://d-apt.sourceforge.net/) to you sources list (on Debian & derivatives) and install
the following packages:

- dmd-bin=2.067.1-0
- libgtkd1-dev

This sould be enough to build the plugin.

# Preparing

Clone the repo:

`~ $ git clone https://jbl@bitbucket.org/jbl/geany-plugin-d.git`

# Compiling

To build the sample plugin:

`geany-plugin-d $ scons`

This will put the plugin shared library by default in the `build/release` folder.

You can then copy the `libgeany-plugin-d.so` file from this folder to `$HOME/.config/geany/plugins` which is where geany will look for user-plugins.
